#Bluetooth Hackday

##Repository Layout

*BluetoothCentralApp* - contains a universal app that is intended to serve as the CBCentral
 
*BluetoothPeripheralApp* - contains an iPhone targetted app that is intended to serve as CBPeripherals

Where possible, perhaps use *CocoaPods* for depenency management.

##Links

- [Core Bluetooth Programming Guide](https://developer.apple.com/library/ios/documentation/NetworkingInternetWeb/Conceptual/CoreBluetooth_concepts/AboutCoreBluetooth/Introduction.html#//apple_ref/doc/uid/TP40013257-CH1-SW1)


