//
//  BHAppDelegate.h
//  BluetoothPeripheralApp
//
//  Created by H.Y. Leung on 2013-10-19.
//  Copyright (c) 2013 n/a. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
